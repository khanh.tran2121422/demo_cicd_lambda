import { Handler } from "aws-lambda"
import dayjs from "dayjs"

export const handler: Handler = async (event, context) => {
  const now = dayjs()
  console.log('Event verison 5: \n', JSON.stringify(event, null, 2))
  return context.logStreamName
}
