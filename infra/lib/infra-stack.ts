import * as cdk from 'aws-cdk-lib';
import { CodePipelineSource, ShellStep } from 'aws-cdk-lib/pipelines';
import { Construct } from 'constructs';

export class InfraStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const helloFunctionLayer = new cdk.aws_lambda.LayerVersion(this, 'HelloFuctionLayer', {
      code: cdk.aws_lambda.Code.fromAsset('../dist_layer'),
      compatibleRuntimes: [cdk.aws_lambda.Runtime.NODEJS_18_X]
    })

    // create hello lambda function
    const helloFuction = new cdk.aws_lambda.Function(this, 'HelloFuction', {
      code: cdk.aws_lambda.Code.fromAsset('../dist'),
      handler: "main.handler",
      runtime: cdk.aws_lambda.Runtime.NODEJS_18_X,
      layers: [helloFunctionLayer]
    })
  }
}

export class PiplineStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props)
    const codestar = new cdk.aws_codestarconnections.CfnConnection(this, "CodeStarConnectionGitLab", {
      connectionName: "HelloGitLabConnection",
      providerType: "GitLab"
    })

    // const cloudFormationRole = new cdk.aws_iam.Role(this, "HelloCloudFormationRole", {
    //  assumedBy: new cdk.aws_iam.ServicePrincipal(''),
    //  description: "Hello cloud formation role"
    // })

    const pipeline = new cdk.pipelines.CodePipeline(this, 'CodePipeline', {
      pipelineName: 'HelloCodePipeline',
      synth: new cdk.pipelines.CodeBuildStep('Synth', {
        input: CodePipelineSource.connection(
          "khanh.tran2121422/demo_cicd_lambda",
          "main",
          {
            connectionArn: codestar.attrConnectionArn
          },
        ),
        installCommands: ["npm install -g aws-cdk"],
        commands: ["pwd", "ls", "npm ci", "npm run build", "cd ${CODEBUILD_SRC_DIR}/dist_layer/nodejs && npm ci && npm run build", "cd ${CODEBUILD_SRC_DIR}/infra && npm ci && npm run build && npm run cdk synth InfraStack"],
        primaryOutputDirectory: 'infra/cdk.out',
      })
    })

    pipeline.addStage(new DeployInfraStage(this, "DeployInfraStack", props))

    // deployStage = cdk.Stage.of({  })
  }
}

class DeployInfraStage extends cdk.Stage {
  constructor(scope: Construct, id: string, props?: cdk.StageProps) {
    super(scope, id, props);

    new cdk.Stack(this, 'InfraStack', props)
  }
}
